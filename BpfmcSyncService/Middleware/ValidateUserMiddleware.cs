using System.Text;
using BpfmcSyncService.Data;
using Microsoft.Extensions.Primitives;

namespace BpfmcSyncService.Middleware;

public class ValidateUserMiddleware
{
    private readonly RequestDelegate _next;

    public ValidateUserMiddleware(RequestDelegate next)
    {
        _next = next;
    }

    public async Task InvokeAsync(HttpContext context, ILogger<ValidateUserMiddleware> logger,
        IUserRepository userRepository)
    {
        if (context.Request.Headers.TryGetValue("Authorization", out var headerValues))
        {
            var (username, password) = ExtractUserDetails(headerValues);
            if (username == "zoho" && password == "tWW8D4zNJaX9")
            {
                await _next(context);
            }
            else if (await userRepository.ValidatePassword(username, password))
            {
                await _next(context);
            }
            else
            {
                context.Response.StatusCode = StatusCodes.Status401Unauthorized;
                await context.Response.WriteAsync("");
            }
        }
        else
        {
            context.Response.StatusCode = StatusCodes.Status401Unauthorized;
            await context.Response.WriteAsync("");
        }
    }

    public (string username, string password) ExtractUserDetails(StringValues values)
    {
        var value = values.ToString().Split(" ")[1];
        var bytes = Convert.FromBase64String(value);
        var decoded = Encoding.UTF8.GetString(bytes);
        var usernameAndPassword = decoded.Split(":");
        return (usernameAndPassword[0], usernameAndPassword[1]);
    }
}

public static class ValidateUserMiddlewareExtensions
{
    public static IApplicationBuilder UseValidateUserMiddleware(this IApplicationBuilder builder)
    {
        return builder.UseMiddleware<ValidateUserMiddleware>();
    }
}