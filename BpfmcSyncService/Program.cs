using BpfmcSyncService.Data;
using BpfmcSyncService.Middleware;
using Microsoft.Extensions.Options;
using MongoDB.Driver;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddAutoMapper(typeof(Program));
builder.Services.Configure<BpfmcSyncMongoSettings>(sp =>
{
    builder.Configuration.Bind(nameof(BpfmcSyncMongoSettings), sp);
});

builder.Services.AddSingleton<IBpfmcSyncMongoSettings>(sp => sp.GetRequiredService<IOptions<BpfmcSyncMongoSettings>>().Value);
builder.Services.AddSingleton<IMongoClient>(sp =>
{
    var settings = sp.GetRequiredService<IBpfmcSyncMongoSettings>();
    return new MongoClient(settings.ConnectionString);
});

builder.Services.AddScoped<IVehicleRepository, VehicleRepository>();
builder.Services.AddScoped<IUserRepository, UserRepository>();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

// app.UseHttpsRedirection();
app.UseValidateUserMiddleware();
app.UseAuthorization();

app.MapControllers();

app.Run();