using System.ComponentModel.DataAnnotations;

namespace BpfmcSyncService.Data;

public class UserDto
{
    [Required]
    public string? Username { get; set; }
    [Required]
    public string? Password { get; set; }
    [Required]
    public string? ApiKey { get; set; }
}