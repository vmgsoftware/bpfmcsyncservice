using AutoMapper;

namespace BpfmcSyncService.Data;

public class VehicleProfile: Profile
{
    public VehicleProfile()
    {
        CreateMap<VehicleDto, Vehicle>().ReverseMap();
        CreateMap<VehicleImageDto, VehicleImage>().ReverseMap();
    }
}

public class UserProfile : Profile
{
    public UserProfile()
    {
        CreateMap<UserDto, User>().ReverseMap();
    }
}