using MongoDB.Driver;

namespace BpfmcSyncService.Data;

public interface IVehicleRepository : IRepository<Vehicle>
{
    Task<IEnumerable<Vehicle>> GetUpdatedFrom(DateTime startDate);
    Task<IEnumerable<Vehicle>> GetByTransferStatus(VehicleTransferStatus status);
    Task<IEnumerable<Vehicle>> GetByTransferStatusForCompany(VehicleTransferStatus status, int companyId);
    Task SetTransferStatus(string id, VehicleTransferStatus status);
}

public class VehicleRepository : IVehicleRepository
{
    private readonly ILogger<VehicleRepository> _logger;
    private readonly IMongoCollection<Vehicle> _vehicles;

    public VehicleRepository(IMongoClient client, ILogger<VehicleRepository> logger)
    {
        _logger = logger;
        var db = client.GetDatabase("bpfmczohosync");
        _vehicles = db.GetCollection<Vehicle>("vehicles");
    }

    public async Task<Vehicle> Create(Vehicle vehicle)
    {
        vehicle.ZohoCode = vehicle.ZohoCode.ToUpper();
        
        await _vehicles.InsertOneAsync(vehicle);

        return vehicle;
    }

    public async Task Update(string id, Vehicle vehicle)
    {
        vehicle.Updated = DateTime.Now;
        vehicle.VehicleTransferStatus = VehicleTransferStatus.Pending;
        var newId = id.ToUpper();
        vehicle.ZohoCode = vehicle.ZohoCode.ToUpper();
        await _vehicles.ReplaceOneAsync(v => v.ZohoCode == newId, vehicle);
    }

    public async Task<IEnumerable<Vehicle>> GetAll()
    {
        var vehicles = await _vehicles.Find(v => v.Deleted == false).ToListAsync();

        return vehicles;
    }

    public async Task<Vehicle?> Get(string id)
    {
        return await _vehicles.Find(v => v.ZohoCode == id.ToUpper()).FirstOrDefaultAsync();
    }

    public async Task Delete(string id)
    {
        await _vehicles.UpdateOneAsync(v => v.ZohoCode == id.ToUpper(),
            Builders<Vehicle>.Update.Set(t => t.Deleted, true).Set(t => t.Updated, DateTime.Now));
    }

    public async Task<IEnumerable<Vehicle>> GetUpdatedFrom(DateTime startDate)
    {
        var vehicles = await _vehicles.Find(Builders<Vehicle>.Filter.Gte(c => c.Updated, startDate)).ToListAsync();

        return vehicles ?? new List<Vehicle>();
    }

    public async Task<IEnumerable<Vehicle>> GetByTransferStatus(VehicleTransferStatus status)
    {
        var vehicles = await _vehicles.Find(Builders<Vehicle>.Filter.Eq(c => c.VehicleTransferStatus, status)).ToListAsync();

        return vehicles ?? new List<Vehicle>();
    }
    
    public async Task<IEnumerable<Vehicle>> GetByTransferStatusForCompany(VehicleTransferStatus status, int companyId)
    {
        var vehicles = await _vehicles.Find(Builders<Vehicle>.Filter.Eq(c => c.VehicleTransferStatus, status)).ToListAsync();
        if (vehicles != null)
            vehicles = vehicles.Where(vehicle => vehicle.CompanyId == companyId).ToList();

        return vehicles ?? new List<Vehicle>();
    }

    public async Task SetTransferStatus(string id, VehicleTransferStatus status)
    {
        await _vehicles.UpdateOneAsync(v => v.ZohoCode == id.ToUpper(),
            Builders<Vehicle>.Update.Set(v => v.VehicleTransferStatus, status));
    }
}