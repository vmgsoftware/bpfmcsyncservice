using System.ComponentModel.DataAnnotations;

namespace BpfmcSyncService.Data;

public class VehicleDto
{
    [Required]
    public string ZohoCode { get; set; } = string.Empty;
    [Required]
    [RegularExpression(@"^\d{8}$", ErrorMessage = "MmCode must be a string of 8 numbers only, left padded if less than 8.")]
    public string MmCode { get; set; } = string.Empty;
    [Range(0, int.MaxValue, ErrorMessage = "Company Id cannot be a negative value.")]
    public int CompanyId { get; set; }
    [Required]
    public string SellerName { get; set; } = string.Empty;
    [Required]
    public string SellerContactNumber { get; set; } = string.Empty;
    [Required]
    public string SellerEmailAddress { get; set; } = string.Empty;
    [Required]
    public string SellerProvince { get; set; } = string.Empty;
    [Required]
    [Range( 0.0, double.MaxValue, ErrorMessage =  "Purchase Price must be a positive number.")]
    public decimal PurchasePrice { get; set; }
    [Required]
    [Range( 0.0, double.MaxValue, ErrorMessage =  "Selling Price must be a positive number.")]
    public decimal SellingPrice { get; set; }
    [Required]
    [Range(1900, 2099, ErrorMessage = "VehicleYear must be greater than 0.")]
    public int VehicleYear { get; set; }
    [Required]
    [Range(1, int.MaxValue, ErrorMessage = "Mileage must be greater than 0.")]
    public int Mileage { get; set; }
    
    public string? SellerSuburb { get; set; }
    public bool? SpareKey { get; set; }
    public DateTime? LicenseDiskExpiry { get; set; }
    public string? RegistrationNumber { get; set; }
    public string? EngineNumber { get; set; }
    public string? VinNumber { get; set; }
    public string? Colour { get; set; }
    public string? Condition { get; set; }
    public decimal? MmTrade { get; set; }
    public decimal? MmRetail { get; set; }
    public string? CarOwner { get; set; }
    public IList<VehicleImageDto>? Images { get; set; }
}

public class VehicleImageDto
{
    public int Position { get; set; }
    public string Url { get; set; } = string.Empty;
}