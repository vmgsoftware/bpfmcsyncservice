namespace BpfmcSyncService.Data;

public interface IDocument
{
    public DateTime Created { get; set; }
    public DateTime Updated { get; set; }
    public bool Deleted { get; set; }
}

public interface IRepository<T> where T : IDocument
{
    Task<T> Create(T entity);
    Task Update(string id, T entity);
    Task<IEnumerable<T>> GetAll();
    Task<T?> Get(string id);
    Task Delete(string id);
}