using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson.Serialization.IdGenerators;
using MongoDB.Driver;

namespace BpfmcSyncService.Data;

public class User: IDocument
{
    [BsonId]
    [BsonRepresentation(BsonType.String)]
    public string Username { get; set; } = string.Empty;
    private string _password = string.Empty;
    public string Password
    {
        get => _password;
        set
        {
            var hashedPassword = BCrypt.Net.BCrypt.HashPassword(value);
            _password = hashedPassword;
        }
    }

    public string ApiKey { get; set; } = string.Empty;
    public DateTime Created { get; set; } = DateTime.Now;
    public DateTime Updated { get; set; } = DateTime.Now;
    public bool Deleted { get; set; } = false;

    public bool IsPasswordValid(string password)
    {
        return BCrypt.Net.BCrypt.Verify(password, _password);
    }
}

public interface IUserRepository : IRepository<User> {

    Task<bool> ValidatePassword(string username, string password);
}

public class UserRepository : IUserRepository
{
    private readonly IMongoCollection<User> _users;

    public UserRepository(IMongoClient client)
    {
        var db = client.GetDatabase("bpfmczohosync");
        _users = db.GetCollection<User>("users");
    }
    
    public async Task<User> Create(User entity)
    {
        await _users.InsertOneAsync(entity);
        return entity;
    }

    public async Task Update(string id, User entity)
    {
        entity.Updated = DateTime.Now;
        await _users.ReplaceOneAsync(i => i.Username == id, entity);
    }

    public Task<IEnumerable<User>> GetAll()
    {
        throw new NotImplementedException();
    }

    public async Task<User?> Get(string id)
    {
        return await _users.Find(i => i.Username == id && i.Deleted == false).FirstOrDefaultAsync();
    }

    public async Task Delete(string id)
    {
        await _users.UpdateOneAsync(i => i.Username == id,
            Builders<User>.Update.Set(f => f.Deleted, true).Set(f => f.Updated, DateTime.Now));
    }

    public async Task<bool> ValidatePassword(string username, string password)
    {
        var user = await _users.Find(i => i.Username == username).FirstOrDefaultAsync();

        return user != null && user.IsPasswordValid(password);
    }
}