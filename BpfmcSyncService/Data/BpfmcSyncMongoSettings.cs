namespace BpfmcSyncService.Data;

public interface IBpfmcSyncMongoSettings
{
    string ConnectionString { get; set; }
}

public class BpfmcSyncMongoSettings : IBpfmcSyncMongoSettings
{
    public string ConnectionString { get; set; } = string.Empty;
}