using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace BpfmcSyncService.Data;

public class Vehicle: IDocument
{
    [BsonId]
    [BsonRepresentation(BsonType.String)]
    public string ZohoCode { get; set; } = string.Empty;
    public string MmCode { get; set; } = string.Empty;
    public int CompanyId { get; set; }
    public string SellerName { get; set; } = string.Empty;
    public string SellerContactNumber { get; set; } = string.Empty;
    public string SellerEmailAddress { get; set; } = string.Empty;
    public string SellerProvince { get; set; } = string.Empty;
    public decimal PurchasePrice { get; set; }
    public decimal SellingPrice { get; set; }
    
    public string? SellerSuburb { get; set; } = string.Empty;
    public int VehicleYear { get; set; }
    public int Mileage { get; set; }
    public bool? SpareKey { get; set; }
    public DateTime? LicenseDiskExpiry { get; set; }
    public string? RegistrationNumber { get; set; }
    public string? EngineNumber { get; set; }
    public string? VinNumber { get; set; }
    public string? Colour { get; set; }
    public string? Condition { get; set; }
    public decimal? MmTrade { get; set; }
    public decimal? MmRetail { get; set; }
    public string? CarOwner { get; set; }
    public IList<VehicleImageDto>? Images { get; set; }
    [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
    public DateTime Created { get; set; } = DateTime.Now;
    [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
    public DateTime Updated { get; set; } = DateTime.Now;

    public bool Deleted { get; set; } = false;

    public VehicleTransferStatus VehicleTransferStatus { get; set; } = VehicleTransferStatus.Pending;
}

public class VehicleImage
{
    public int Position { get; set; }
    public Uri Url { get; set; } = new Uri("");
}

public enum VehicleTransferStatus
{
    Pending,
    Transferred
}