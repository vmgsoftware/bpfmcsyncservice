using AutoMapper;
using BpfmcSyncService.Data;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Driver;

namespace BpfmcSyncService.Controllers;

[ApiController]
[Route("api/v1/[controller]")]
public class VehiclesController : ControllerBase
{
    private readonly IVehicleRepository _repository;
    private readonly IMapper _mapper;
    private readonly ILogger<VehiclesController> _logger;

    public VehiclesController(IVehicleRepository repository, IMapper mapper, ILogger<VehiclesController> logger)
    {
        _repository = repository;
        _mapper = mapper;
        _logger = logger;
    }

    [HttpPost]
    public async Task<IActionResult> Create(VehicleDto vehicle)
    {
        try
        {
            if (vehicle.CompanyId == 0)
                vehicle.CompanyId = 156;
            
            var model = _mapper.Map<Vehicle>(vehicle);
            model = await _repository.Create(model);
            var updateModel = _mapper.Map<VehicleDto>(model);
            return CreatedAtRoute("Get", routeValues: new { id = model.ZohoCode }, updateModel);
        }
        catch (MongoWriteException e)
        {
            if (e.WriteError.Category == ServerErrorCategory.DuplicateKey)
            {
                return Problem(statusCode: StatusCodes.Status422UnprocessableEntity,
                    detail: $"Zoho Code: {vehicle.ZohoCode} already exists.");
            }
        }

        return Problem();
    }

    [HttpGet("{id}", Name = "Get")]
    public async Task<IActionResult> Get(string id)
    {
        try
        {
            var model = await _repository.Get(id);
            if (model == null)
            {
                return NotFound();
            }

            var vm = _mapper.Map<VehicleDto>(model);
            return Ok(vm);
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
            return Problem();
        }
    }

    [HttpGet]
    public async Task<IEnumerable<VehicleDto>> GetAll()
    {
        var vehicles = await _repository.GetAll();
        var vm = _mapper.Map<IEnumerable<VehicleDto>>(vehicles);

        return vm;
    }

    [HttpGet("from/{startDate:datetime}")]
    public async Task<IEnumerable<VehicleDto>> GetUpdateFrom(DateTime startDate)
    {
        var vehicles = await _repository.GetUpdatedFrom(startDate);
        var vm = _mapper.Map<IEnumerable<VehicleDto>>(vehicles);

        return vm;
    }


    [HttpPut("{id}")]
    public async Task<IActionResult> Update(string id, VehicleDto vehicle)
    {
        try
        {
            if (!vehicle.ZohoCode.Equals(id, StringComparison.InvariantCultureIgnoreCase))
                return Problem(statusCode: StatusCodes.Status400BadRequest,
                    detail: "The ZohoCode cannot be changed once created.");
            
            var existingVehicle = await _repository.Get(id);
            if (existingVehicle == null)
            {
                return Problem(statusCode: StatusCodes.Status400BadRequest,
                    detail: "The ZohoCode does not exist for any vehicle.");
            }

            if (vehicle.CompanyId == 0)
                vehicle.CompanyId = existingVehicle.CompanyId;

            var model = _mapper.Map<Vehicle>(vehicle);
            await _repository.Update(id, model);
            return NoContent();
        }
        catch (Exception e)
        {
            return Problem();
        }
    }

    [HttpDelete("{id}")]
    public async Task<IActionResult> Delete(string id)
    {
        await _repository.Delete(id);
        return Ok();
    }

    [HttpGet("status/pending")]
    public async Task<IEnumerable<VehicleDto>> GetPendingVehicles()
    {
        var vehicles = await _repository.GetByTransferStatus(VehicleTransferStatus.Pending);
        var vm = _mapper.Map<IEnumerable<VehicleDto>>(vehicles);
        return vm;
    }
    
    [HttpGet("status/pending/{companyId}")]
    public async Task<IEnumerable<VehicleDto>> GetPendingVehiclesByCompanyId(int companyId)
    {
        var vehicles = await _repository.GetByTransferStatusForCompany(VehicleTransferStatus.Pending, companyId);
        var vm = _mapper.Map<IEnumerable<VehicleDto>>(vehicles);
        return vm;
    }

    [HttpPut("{id}/transferred")]
    public async Task<IActionResult> Update(string id)
    {
        await _repository.SetTransferStatus(id, VehicleTransferStatus.Transferred);
        return Ok();
    }
}