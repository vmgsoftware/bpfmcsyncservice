using AutoMapper;
using BpfmcSyncService.Data;
using Microsoft.AspNetCore.Mvc;

namespace BpfmcSyncService.Controllers;

[ApiController]
[Route("api/v1/[controller]")]
public class UsersController : ControllerBase
{
    private readonly IUserRepository _userRepository;
    private readonly IMapper _mapper;

    public UsersController(IUserRepository userRepository, IMapper mapper)
    {
        _userRepository = userRepository;
        _mapper = mapper;
    }

    [HttpPost]
    public async Task<IActionResult> Create(UserDto userDto)
    {
        var user = _mapper.Map<User>(userDto);

        user = await _userRepository.Create(user);

        var result = _mapper.Map<UserDto>(user);

        return Ok(result);
    }

    [HttpGet("{id}")]
    public async Task<UserDto> Get(string id)
    {
        var user = await _userRepository.Get(id);
        var userDto = _mapper.Map<UserDto>(user);

        return userDto;
    }
}